package slip

import (
	"bufio"
	"bytes"
	"io"
)

const (
	END     = 0300 // indicates end of packet
	ESC     = 0333 // indicates byte stuffing
	ESC_END = 0334 // ESC ESC_END means END data byte
	ESC_ESC = 0335 // ESC ESC_ESC means ESC data byte
)

type Decoder struct {
	br     *bufio.Reader
	synced bool
}

type Encoder struct {
	bw *bufio.Writer
}

func NewDecoder(r io.Reader) *Decoder {
	if v, ok := r.(*bufio.Reader); ok {
		return &Decoder{br: v}
	}
	return &Decoder{br: bufio.NewReader(r)}
}

func NewEncoder(w io.Writer) *Encoder {
	if v, ok := w.(*bufio.Writer); ok {
		return &Encoder{bw: v}
	}
	return &Encoder{bw: bufio.NewWriter(w)}
}

func (sd *Decoder) Decode() ([]byte, error) {

	var pkt bytes.Buffer

	for {
		c, err := sd.br.ReadByte()
		if err != nil {
			return nil, err
		}

		if c == END {
			if !sd.synced {
				pkt.Reset()
				sd.synced = true
			}
			if pkt.Len() > 0 {
				return pkt.Bytes(), nil
			}
			continue
		}

		// if ESC, read the next byte and let
		// the default case append it for us
		if c == ESC {
			if c, err = sd.br.ReadByte(); err != nil {
				return nil, err
			}
			if c == ESC_END {
				c = END
			} else if c == ESC_ESC {
				c = ESC
			}
		}

		// default case
		if err := pkt.WriteByte(c); err != nil {
			return nil, err
		}
	}
}

func (se *Encoder) Encode(msg []byte) error {

	// sync our receiver if they haven't seen an END yet
	if err := se.bw.WriteByte(END); err != nil {
		return err
	}

	for _, c := range msg {
		switch c {
		case END:
			endBytes := []byte{ESC, ESC_END}
			if _, err := se.bw.Write(endBytes); err != nil {
				return err
			}

		case ESC:
			escBytes := []byte{ESC, ESC_ESC}
			if _, err := se.bw.Write(escBytes); err != nil {
				return err
			}

		default:
			if err := se.bw.WriteByte(c); err != nil {
				return err
			}
		}
	}

	// real END byte
	if err := se.bw.WriteByte(END); err != nil {
		return err
	}

	return se.bw.Flush()
}
