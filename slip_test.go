package slip

import (
	"bytes"
	"testing"
)

func TestRoundTrip(t *testing.T) {

	cases := [][]byte{
		{0x0, 0x1, 0x2, 0x3},
		{0x0, 0x1, 0x2, 0x3, END, ESC},
		{0x0, ESC_END, END, ESC_ESC, ESC_ESC},
	}

	for _, in := range cases {

		var buf bytes.Buffer

		enc := NewEncoder(&buf)
		if err := enc.Encode(in); err != nil {
			t.Errorf("enc err %q", err)
		}

		dec := NewDecoder(&buf)
		out, err := dec.Decode()
		if err != nil {
			t.Errorf("dec err %q", err)
		}

		if !bytes.Equal(in, out) {
			t.Errorf("RoundTrip got %q, want %q", out, in)
		}
	}
}
